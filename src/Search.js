import React from "react";
import Image from "./Image";
import Loader from "./Loader";
import { Navigate } from "react-router-dom";
import Tags from "./Tags";
import Header from "./Header";

const API_KEY = "02ec6bc6ba4d8712561d2064ba08a0e5";

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: "",
            invalidSearch: null,
            photos: [],
            noPhotoFound: null,
            searchedImage: null,
            isLoading: false,
            apiError: false
        };
    }

    setIsLoading(value) {
        this.setState({
            isLoading: value
        })
    }

    handleInputChange = (event) => {
        this.setState({
            search: event.target.value
        });
    };

    handleTagChange = (event) => {
        // console.log(event.target.innerText);
        this.setState({
            search: event.target.innerText
        }, this.doSearch);
        // setState is not synchronous, react batches multiple setState and executes them at once
        // passing second parameter ensures that it will run after the state is changed
    }

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.search.trim().length === 0) {
            this.setState({
                invalidSearch: "Please enter in the input field below",
                photos: [],
                searchedImage: null,
                isLoading: false
            })
        } else {
            this.doSearch();
        }
    };

    doSearch = () => {
        this.setIsLoading(true);
        // nojsoncallback will return raw data json
        // https://www.flickr.com/services/api/flickr.photos.search.html
        fetch(`https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${API_KEY}&tags=${this.state.search.trim()}&per_page=25&format=json&nojsoncallback=1`)
            .then((response) => {
                if (response.ok) {
                    console.log("Success fetch");
                    this.setState({
                        apiError: false
                    });
                    return response.json();
                } else {
                    console.log("Not successful fetch");
                    // navigate to error page
                    this.setIsLoading(false);
                    this.setState({
                        apiError: true
                    });
                }
            })
            .then((data) => {
                // console.log(data.photos.photo);
                if (data.photos.photo.length === 0) {
                    this.setState({
                        invalidSearch: null,
                        noPhotoFound: "We can't find the image you're looking for... please search something else",
                        photos: [],
                        searchedImage: null,
                        isLoading: false
                    });
                } else {
                    let photos = [];
                    photos = data.photos.photo.map((photo) => {
                        // api doc for this https://www.flickr.com/services/api/misc.urls.html
                        let url = `https://live.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_q.jpg`;

                        return url;
                    });
                    this.setState({
                        invalidSearch: null,
                        noPhotoFound: null,
                        photos: photos,
                        searchedImage: this.state.search + " images"

                    })
                    console.log(photos);
                    // loader here
                    this.setIsLoading(false);
                }


            })
            .catch((error) => {
                console.log("Error fetch", error);
                // navigate to error page
                this.setIsLoading(false);
                this.setState({
                    apiError: true
                });
            });
        console.log(this.state.search);
    }

    render() {
        return (
            <React.Fragment>
                <Header />

                {this.state.apiError ?
                    <Navigate to="/error" replace={true} />
                    :
                    null
                }

                <p>{this.state.invalidSearch}</p>

                <main>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text"
                            placeholder="Search something... like pizza"
                            onChange={this.handleInputChange}
                            value={this.state.search}
                        >
                        </input>

                        <button type="submit">
                            <img className="search-icon" src="./search.svg"></img>
                        </button>
                    </form>
                </main>

                <Tags tagList={["Mountain", "Lake", "Birds", "Food"]}
                    tagChange={this.handleTagChange} />

                <h1 className="noPhotoFound">{this.state.noPhotoFound}</h1>

                {this.state.isLoading ?
                    <Loader />
                    :
                    <>
                        <h1 className="searched-image">{this.state.searchedImage}</h1>
                        <div className="image-gallery">
                            {this.state.photos.map((photo) => {
                                return (
                                    <Image key={photo} photo={photo} alt="image" />
                                );
                            })}
                        </div>
                    </>
                }
            </React.Fragment>
        );
    }
}


export default Search;