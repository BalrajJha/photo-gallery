import React from "react";

class Image extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div>
                <img src={this.props.photo} />
            </div>
        );
    };
}

export default Image;